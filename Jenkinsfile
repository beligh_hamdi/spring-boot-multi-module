#!groovy

def shouldAddProfile(branch) {
    result = ""
    if (branch ==~ /(^feature\/.*)|(fix\/.*)/) {
        result = "-P add-branch-classifier"
    }
    return result
}

def getAction() {
    if (env.CHANGE_ID == null) {
        return "package"  //"deploy"
    } else {
        return "package"
    }
}


def jobs = ["core-module", "web-module"]

def parallelStagesMap = jobs.collectEntries {
    ["Stage: ${it}" : generateStage(it)]
}

def generateStage(job) {
    mvn_profile = shouldAddProfile(BRANCH_NAME)
    mvn_action = getAction()
    mvn_opts = "${mvn_profile}"
    return {
        stage("stage: ${job}") {
            echo "Job: ${job} started...."
            withMaven(maven: 'M3') {
                sh "cd ${job} && mvn -U -B clean ${mvn_action} ${mvn_opts}"
            }
        }
    }
}

pipeline {
    agent any

    tools {
        maven 'M3'
        jdk 'JDK8'
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '1'))
    }

    stages {
        stage('Stage Global') {
            steps {
                withMaven(maven: 'M3') {
                    sh "mvn -U -B clean ${mvn_action} ${mvn_opts}"
                }
            }
        }
        stage('Pipline Parallel Stage') {
            failFast true
            steps {
                script {
                    parallel parallelStagesMap
                }
            }
        }
    }
}